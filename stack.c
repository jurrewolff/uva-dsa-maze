#include <stdio.h>
#include <stdlib.h>

#include "stack.h"

int stack_full(const struct stack *s);

struct stack {
  int *elems;
  size_t capacity;
  int top;

  unsigned int pushes;
  unsigned int pops;
  unsigned int max;
};

// Doubles stack elem member size.
//
// s: The stack to operate on.
//
// Returns: 0 on succes or -1 on failure.
int resize_stack(struct stack *s) {
  s->capacity = s->capacity <= 0 ? 10 : s->capacity * 2;
  int *newElems = malloc(sizeof(int) * s->capacity);
  if (newElems == NULL) {
    return -1;
  }

  for (int i = 0; i < (int)s->capacity / 2; ++i) {
    newElems[i] = s->elems[i];
  }

  free(s->elems);
  s->elems = NULL;

  s->elems = newElems;

  return 0;
}

// Initialize new stack members to default values.
//
// capacity: The maximum number of elements that are allowed on the stack.
//
// Returns: Initialized stack object or NULL if memory allocation fails.
struct stack *stack_init(size_t capacity) {
  struct stack *s = malloc(sizeof(struct stack));
  if (s == NULL) {
    return NULL;
  }

  if (capacity == 0) {
    capacity = 10;
  }

  s->elems = malloc(sizeof(int) * capacity);
  if (s->elems == NULL) {
    free(s);
    s = NULL;
    return NULL;
  }

  s->capacity = capacity;
  s->top = -1;

  s->pushes = 0;
  s->pops = 0;
  s->max = 0;

  return s;
}

// Free allocated memory associated with the stack object.
//
// s: The stack to operate on.
void stack_cleanup(struct stack *s) {
  free(s->elems);
  s->elems = NULL;
  free(s);
  s = NULL;
}

// Print statistics of stack pushes, pops and max number of elements to stderr.
//
// s: The stack to operate on.
void stack_stats(const struct stack *s) {
  if (s == NULL) {
    return;
  }

  fprintf(stderr, "stats %d %d %d\n", s->pushes, s->pops, s->max);
}

// Add new element to top of stack. Resize elem member if necessary.
//
// s: The stack to operate on.
// c: Value to add to stack.
//
// Returns: 1 If stack is full or 0 on success.
int stack_push(struct stack *s, int c) {
  if (s == NULL) {
    return 1;
  }

  if (stack_full(s)) {
    if (resize_stack(s) == -1) {
      return 1;
    }
  }

  s->top++;
  s->elems[s->top] = c;
  s->pushes++;

  if (s->top >= 0 && (unsigned int)s->top + 1 > s->max) {
    s->max = (unsigned int)s->top + 1;
  }

  return 0;
}

// Get top element from stack, whilst removing it.
//
// s: The stack to operate on.
//
// Returns: Value of top stack element or -1 if stack is empty.
int stack_pop(struct stack *s) {
  if (s == NULL || stack_empty(s)) {
    return -1;
  }

  int e = s->elems[s->top];
  s->top--;
  s->pops++;
  return e;
}

// Get top element from stack.
//
// s: The stack to operate on.
//
// Returns: Value of top stack element or -1 if stack is empty.
int stack_peek(const struct stack *s) {
  if (s == NULL || stack_empty(s)) {
    return -1;
  }

  return s->elems[s->top];
}

// Check if stack is empty.
//
// s: The stack to operate on.
//
// Returns: 1 If stack is empty or 0 if there's elements present.
int stack_empty(const struct stack *s) {
  if (s == NULL) {
    return -1;
  }

  if (s->top == -1) {
    return 1;
  } else {
    return 0;
  }
}

// Check if stack is full.
//
// s: The stack to operate on.
//
// Returns: 1 If stack is full or 0 if there's space left.
int stack_full(const struct stack *s) {
  if (s == NULL) {
    return -1;
  }

  if (s->top >= 0 && s->top == ((int)s->capacity - 1)) {
    return 1;
  } else {
    return 0;
  }
}

// Get the number of elements present in stack.
//
// s: The stack to operate on.
//
// Returns: Number of elements in stack.
size_t stack_size(const struct stack *s) {
  if (s->top >= 0) {
    return ((unsigned long)s->top + 1);
  } else {
    return 0;
  }
}
