#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

#include "maze.h"
#include "stack.h"

#define NOT_FOUND -1
#define ERROR -2

#define INIT_BUF_SIZ 8192

struct node {
  int loc;
  struct node *parent;
};

// Clean-up nodes starting from n and finishing when n->parent is NULL or if
// specified target_loc has been reached.
//
// n: The node from where to start cleaning up.
// target_loc: Target location up to which point nodes will be freed. Set to -1
// when irrelevant (i.e. freeing up until last node).
void node_cleanup(struct node *n, int target_loc) {
  struct node *temp_node = NULL;
  while ((temp_node = n) != NULL) {
    if (temp_node->loc == target_loc) {
      return;
    }

    n = n->parent;
    free(temp_node);
    temp_node = NULL;
  }
}

// Traces path back from node n to first node and marks locations in maze.
//
// m: The maze the function will operate on.
// n: The node from where to start tracing back.
//
// Returns: Length of traced path.
int trace_path(struct maze *m, struct node *n) {
  int steps = 0;
  struct node *current_node = n;
  while (current_node->parent != NULL) {
    int loc = current_node->loc;
    maze_set(m, maze_row(m, loc), maze_col(m, loc), PATH);

    current_node = current_node->parent;
    steps++;
  }
  return steps + 1;
}

/* Solves the maze m.
 * Returns the length of the path if a path is found.
 * Returns NOT_FOUND if no path is found and ERROR if an error occured.
 */
int dfs_solve(struct maze *m) {
  if (!maze_at_start(m, 1, 1)) {
    return ERROR;
  }

  struct stack *s = stack_init(INIT_BUF_SIZ);
  if (s == NULL) {
    return ERROR;
  }

  struct stack *junctions = stack_init(INIT_BUF_SIZ);
  if (junctions == NULL) {
    return ERROR;
  }

  int loc = maze_index(m, 1, 1);
  stack_push(s, loc);

  struct node *last_node = NULL;
  while (!stack_empty(s)) {
    loc = stack_pop(s);
    maze_set(m, maze_row(m, loc), maze_col(m, loc), VISITED);

    int cur_row = maze_row(m, loc);
    int cur_col = maze_col(m, loc);
    if (maze_at_destination(m, cur_row, cur_col)) {
      int steps = trace_path(m, last_node);

      node_cleanup(last_node, -1);
      stack_cleanup(s);
      stack_cleanup(junctions);
      return steps;
    }

    struct node *n = malloc(sizeof(struct node));
    n->loc = loc;
    n->parent = last_node;
    last_node = n;

    int valid_directions = 0;
    for (int i = 0; i < N_MOVES; ++i) {
      int next_row = cur_row + m_offsets[i][0];
      int next_col = cur_col + m_offsets[i][1];

      if (maze_get(m, next_row, next_col) == FLOOR &&
          maze_valid_move(m, next_row, next_col)) {
        maze_set(m, next_row, next_col, TO_VISIT);
        stack_push(s, maze_index(m, next_row, next_col));
        valid_directions++;
      }
    }

    if (valid_directions > 1) {
      // Add junction as many times as there's valid directions, so that each
      //  direction can pop back to the same junction in case of incorrect path.
      for (int i = 0; i < valid_directions - 1; ++i) {
        stack_push(junctions, last_node->loc);
      }
    }

    if (valid_directions == 0) {
      struct node *dead_end_node = last_node;
      int last_junction_loc = stack_pop(junctions);
      while (last_node->loc != last_junction_loc) {
        last_node = last_node->parent;
      }

      node_cleanup(dead_end_node, last_junction_loc);
    }
  }

  node_cleanup(last_node, -1);
  stack_cleanup(s);
  stack_cleanup(junctions);
  return NOT_FOUND;
}

int main(void) {
  /* read maze */
  struct maze *m = maze_read();
  if (!m) {
    printf("Error reading maze\n");
    return 1;
  }

  /* solve maze */
  int path_length = dfs_solve(m);
  if (path_length == ERROR) {
    printf("dfs failed\n");
    maze_cleanup(m);
    return 1;
  } else if (path_length == NOT_FOUND) {
    printf("no path found from start to destination\n");
    maze_cleanup(m);
    return 1;
  }
  printf("dfs found a path of length: %d\n", path_length);

  /* print maze */
  maze_print(m, false);
  maze_output_ppm(m, "out.ppm");

  maze_cleanup(m);
  return 0;
}
