#include <stdbool.h>
#include <stdio.h>

#include "maze.h"
#include "queue.h"
#include "stack.h"

#define NOT_FOUND -1
#define ERROR -2

#define BUF_SIZ 524288
#define INIT_BUF_SIZ 8192

// Traces path back from loc to start.
//
// m: The maze the function will operate on.
// len: Length of first dimension of loc_parents.
// loc_parents: A 2d array, which must be an array of (parent, loc) pairs.
// loc: The starting location from where to trace back.
//
// Returns: Length of path or -1 on error.
int trace_path(struct maze *m, int len, int loc_parents[len][2], int loc) {
  int steps = 0;
  int parent = loc;
  for (int i = 0; i < len; ++i) {
    for (int j = 0; j < len; ++j) {
      if (loc_parents[j][1] == parent) {
        parent = loc_parents[j][0];
        int p_row = maze_row(m, parent);
        int p_col = maze_col(m, parent);

        if (!maze_valid_move(m, p_row, p_col)) {
          continue;
        } else if (maze_at_start(m, p_row, p_col)) {
          return steps + 1;
        }

        maze_set(m, p_row, p_col, PATH);
        steps++;
      }
    }
  }
  return -1;
}

/* Solves the maze m.
 * Returns the length of the path if a path is found.
 * Returns NOT_FOUND if no path is found and ERROR if an error occured.
 */
int bfs_solve(struct maze *m) {
  if (!maze_at_start(m, 1, 1)) {
    return ERROR;
  }

  struct queue *q = queue_init(INIT_BUF_SIZ);
  if (q == NULL) {
    return ERROR;
  }

  int loc_parents[BUF_SIZ][2]; // TODO - Dynamic instead of BUF_SIZ
  int loc = maze_index(m, 1, 1);
  queue_push(q, loc);

  int count = 0;
  while (!queue_empty(q)) {
    loc = queue_pop(q);

    int cur_row = maze_row(m, loc);
    int cur_col = maze_col(m, loc);
    if (maze_at_destination(m, cur_row, cur_col)) {
      int path_len = trace_path(m, count, loc_parents, loc);
      if (path_len == -1) {
        return ERROR;
      }
      queue_cleanup(q);
      return path_len;
    }

    for (int i = 0; i < N_MOVES; ++i) {
      int next_row = cur_row + m_offsets[i][0];
      int next_col = cur_col + m_offsets[i][1];

      if (maze_get(m, next_row, next_col) == FLOOR &&
          maze_valid_move(m, next_row, next_col)) {
        maze_set(m, next_row, next_col, VISITED);
        queue_push(q, maze_index(m, next_row, next_col));
        loc_parents[count][0] = loc;
        loc_parents[count][1] = maze_index(m, next_row, next_col);
        count++;
      }
    }
  }

  queue_cleanup(q);
  return NOT_FOUND;
}

int main(void) {
  /* read maze */
  struct maze *m = maze_read();
  if (!m) {
    printf("Error reading maze\n");
    return 1;
  }

  /* solve maze */
  int path_length = bfs_solve(m);
  if (path_length == ERROR) {
    printf("bfs failed\n");
    maze_cleanup(m);
    return 1;
  } else if (path_length == NOT_FOUND) {
    printf("no path found from start to destination\n");
    maze_cleanup(m);
    return 1;
  }
  printf("bfs found a path of length: %d\n", path_length);

  /* print maze */
  maze_print(m, false);
  maze_output_ppm(m, "out.ppm");

  maze_cleanup(m);
  return 0;
}
