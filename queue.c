#include <stdio.h>
#include <stdlib.h>

#include "queue.h"

int queue_full(const struct queue *q);

struct queue {
  int *elems;
  size_t capacity;
  int front;
  int rear;

  unsigned int pushes;
  unsigned int pops;
  unsigned int max;
};

// Doubles queue elem member size.
//
// q: The queue to operate on.
//
// Returns: 0 on succes or -1 on failure.
int resize_queue(struct queue *q) {
  q->capacity = q->capacity <= 0 ? 10 : q->capacity * 2;
  int *newElems = malloc(sizeof(int) * q->capacity);
  if (newElems == NULL) {
    return -1;
  }

  for (int i = 0; i < (int)q->capacity / 2; ++i) {
    newElems[i] = q->elems[i];
  }

  free(q->elems);
  q->elems = NULL;

  q->elems = newElems;

  return 0;
}

// Initialize new queue members to default values.
//
// capacity: The maximum number of elements that are allowed in the queue.
//
// Returns: Initialized queue object or NULL if memory allocation fails.
struct queue *queue_init(size_t capacity) {
  struct queue *q = malloc(sizeof(struct queue));
  if (q == NULL) {
    return NULL;
  }

  if (capacity == 0) {
    capacity = 10;
  }

  q->elems = malloc(sizeof(int) * capacity);
  if (q->elems == NULL) {
    free(q);
    q = NULL;
    return NULL;
  }

  q->front = -1;
  q->rear = -1;
  q->capacity = capacity;
  q->pushes = 0;
  q->pops = 0;
  q->max = 0;

  return q;
}

// Free allocated memory associated with the queue object.
//
// q: The queue to operate on.
void queue_cleanup(struct queue *q) {
  free(q->elems);
  q->elems = NULL;
  free(q);
  q = NULL;
}

// Print statistics of queue pushes, pops and max number of elements to stderr.
//
// q: The queue to operate on.
void queue_stats(const struct queue *q) {
  if (q == NULL) {
    return;
  }

  fprintf(stderr, "stats %d %d %d\n", q->pushes, q->pops, q->max);
}

// Add new element to end of queue. Resize elem member if necessary.
//
// q: The queue to operate on.
// e: Value to add to queue.
//
// Returns: 1 If queue is full or 0 on success.
int queue_push(struct queue *q, int e) {
  if (q == NULL) {
    return 1;
  }

  if (queue_empty(q)) {
    q->front = 0;
  }

  if (queue_full(q)) {
    if (resize_queue(q) == -1) {
      return 1;
    }
  }

  q->rear++;
  q->elems[q->rear] = e;
  q->pushes++;

  if ((q->rear - q->front) + 1 >= 0 &&
      (unsigned int)((q->rear - q->front) + 1) > q->max) {
    q->max = (unsigned int)(q->rear - q->front) + 1;
  }

  return 0;
}

// Get front element from queue, whilst removing it.
//
// q: The queue to operate on.
//
// Returns: Value of top queue element or -1 if queue is empty.
int queue_pop(struct queue *q) {
  if (q == NULL || queue_empty(q)) {
    return -1;
  }

  int e = q->elems[q->front];

  if (q->front >= 0 &&
      (q->front == ((int)q->capacity - 1) || q->front >= q->rear)) {
    q->front = -1;
    q->rear = -1;
  } else {
    q->front++;
  }

  q->pops++;

  return e;
}

// Get front element from queue.
//
// q: The queue to operate on.
//
// Returns: Value of top queue element or -1 if queue is empty.
int queue_peek(const struct queue *q) {
  if (q == NULL || queue_empty(q)) {
    return -1;
  } else {
    return q->elems[q->front];
  }
}

// Check if queue is empty.
//
// s: The queue to operate on.
//
// Returns: 1 If queue is empty or 0 if there's elements present.
int queue_empty(const struct queue *q) {
  if (q == NULL) {
    return -1;
  }

  if (q->front == -1) {
    return 1;
  } else {
    return 0;
  }
}

// Check if queue is full.
//
// q: The queue to operate on.
//
// Returns: 1 If queue is full or 0 if there's space left.
int queue_full(const struct queue *q) {
  if (q == NULL) {
    return -1;
  }

  if (q->rear >= 0 && q->rear == ((int)q->capacity - 1)) {
    return 1;
  } else {
    return 0;
  }
}

// Get the number of elements present in queue.
//
// q: The queue to operate on.
//
// Returns: Number of elements in queue.
size_t queue_size(const struct queue *q) {
  if ((q->rear - q->front) + 1 >= 0) {
    return (unsigned long)(q->rear - q->front) + 1;
  } else {
    return 0;
  }
}
